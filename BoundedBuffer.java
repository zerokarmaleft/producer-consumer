//
// BoundedBuffer.java
//

public class BoundedBuffer<T> {
    private static final int DEFAULT_BUFFER_SIZE = 5;

    private final T[] buffer;
    private int tail;
    private int head;
    private int count;

    public BoundedBuffer() {
        this(DEFAULT_BUFFER_SIZE);
    }

    @SuppressWarnings({"unchecked"})
    public BoundedBuffer(int capacity) {
        this.buffer = (T[]) new Object[capacity];
    }

    public synchronized final boolean isFull() {
        return count == buffer.length;
    }

    public synchronized final boolean isEmpty() {
        return count == 0;
    }

    public synchronized void insert(T value) throws InterruptedException {
        while (isFull()) {
            System.out.println(Thread.currentThread()
                               + " waiting for buffer to not be full.");
            wait();
            System.out.println(Thread.currentThread()
                               + " notified: buffer is no longer full.");
        }

        boolean wasEmpty = isEmpty();
        _insert(value);
        _printOperation("insert", value);

        if (wasEmpty)
            notifyAll();
    }

    public synchronized T remove()
        throws InterruptedException {
        while (isEmpty()) {
            System.out.println(Thread.currentThread()
                               + " waiting for buffer to not be empty.");
            wait();
            System.out.println(Thread.currentThread()
                               + " notified: buffer is no longer empty.");
        }

        boolean wasFull = isFull();
        T value = _remove();
        _printOperation("remove", value);

        if (wasFull)
            notifyAll();

        return value;
    }

    private synchronized final void _insert(T value) {
        buffer[tail] = value;
        if (++tail == buffer.length)
            tail = 0;
        count++;
    }

    private synchronized final T _remove() {
        T value = buffer[head];
        buffer[head] = null;
        if (++head == buffer.length)
            head = 0;
        count--;

        return value;
    }

    private synchronized final void _printOperation(String op, T value) {
        System.out.println("==============================================================================");

        /* print operation */
        System.out.println("* " + Thread.currentThread()
                           + " " + op + " "
                           + value + ".");

        /* print buffer contents */
        System.out.print("Buffer: ");
        for (int i = 0; i < buffer.length; i++) {
            if (i == 0) {
                System.out.print("(" + buffer[i] + ", ");
            } else if (i == buffer.length - 1) {
                System.out.println(buffer[i] + ")");
            } else {
                System.out.print(buffer[i] + ", ");
            }
        }

        System.out.println("==============================================================================");
    }
}
