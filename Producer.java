//
// Producer.java
//

import java.util.Random;

public class Producer implements Runnable {
    private Random generator;
    private BoundedBuffer<Integer> buffer;
    private Thread thread;

    public Producer(String name, Random generator,
                    BoundedBuffer<Integer> buffer) {
        this.generator = generator;
        this.buffer = buffer;
        this.thread = new Thread(this, name);
        this.thread.start();
    }

    public void run() {
        /* produce and insert random numbers in bounded buffer forever */
        while (true) {
            try {
                int n = generator.nextInt();
                buffer.insert(n);
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted.");
            }
        }
    }
}
