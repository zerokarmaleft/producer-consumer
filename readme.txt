Edward Cho
Assignment 5: Programming assignment on process synchronization
CS 4323
Dr. Michel Toulouse
April 2, 2012

===============  Introduction  ===============

This program demonstrates the Producer/Consumer pattern described in
section 6.6.1 of the textbook with the project being outlined as
programming project 6.40 at the end of the chapter. Instead of using
semaphores or mutexes to provide locking, this program uses Java
monitors which differs slightly from the Pthreads and Win32 monitor
implementations.

I wrote and built this program using Java 1.6.0_29 on Mac OSX 10.7. I
also tested the program to build and execute correctly on CSX as well.
I have included a Makefile for easy compiling. Simply execute `make`
to build a jarfile (assignment-5.jar):

$ make

and execute with -jar and appropriate command-line arguments:

$ java -jar assignment-5.jar <time to sleep for main thread> <# producer
threads> <# consumer threads>

* Note: The default size of the buffer is set to 5, like the PThreads
example in the textbook. This size can be increased by changing
DEFAULT_BUFFER_SIZE in BoundedBuffer.java.

=============== Implementation ===============

The Producer/Consumer pattern described in 6.40 uses a bounded buffer
to share work units. The Producers put work units into the buffer and
the Consumers remove them. The bounded buffer is implemented as an
array-based circular queue in BoundedBuffer.java. The insert and
remove operations are dependent on the state of the queue. Producers
cannot insert an item if the queue is full. Likewise, Consumers cannot
remove an item if the queue is empty. In a single-threaded program,
these failing conditions would simply mean a failure of the entire
program. In a multi-threaded program, the state of the queue can be
changed by other threads. A full queue may have an empty slot a few
CPU cycles later. An empty queue may have a new item a few CPU cycles
later. Java's built-in monitor mechanism allows us to instruct threads
to wait until a specified condition becomes true before proceeding.

BoundedBuffer.java implements two conditions of the queue - isFull()
and isEmpty(). Both insert() and remove() operations are patterned
after the following pseudocode block:

acquire lock on queue state
while (precondition is false) {
      release lock on queue state
      wait until the precondition might be true
      acquire lock on queue state
}
apply operation
release lock

The Pthreads implementation from the book is rather inefficient since
each thread polls the precondition and sleeps for a random amount of
time if it does not hold. The amount of time each thread sleeps before
polling again is a configuration choice that trades off responsiveness
and CPU usage. The shorter threads sleep, the more responsive they are
to a change in the queue state. However, they also poll more
frequently, consuming more CPU resources. Java defines intrinsic
monitor methods - wait(), notify(), and notifyAll() - for every
object. Since Java monitors are built around notification, threads do
not poll and hence are more efficient.

In insert(), a Producer thread loops to check if the buffer is full.
If it is, then the thread releases the lock and hopefully waits a
Consumer thread to remove an item from the buffer and call
notifyAll(). Because all threads waiting on the monitor are awakened,
there is no guarantee that the precondition that woke up the Producer
thread will be true so that it can proceed. It's possible *another,
different* Producer thread that was also waiting may have inserted an
item, once again changing the state of the buffer to full. So, the
while loop ensures that it is the first thread notified before
proceeding with insertion of a new item. The Producer thread than
calls notifyAll() to wake up all threads waiting on the buffer's
monitor. The remove() method works in very much the same way for
Consumers.

Java defines every object to have an intrinsic lock, which can be used
by specifying the synchronized keyword. Any method (insert() and
remove(), in particular) that may alter the object state should be
synchronized so that the object state remains consistent through
locking. The private methods _insert() and _remove() manipulate the
circular queue by modifying or null'ing array elements and
appropriately sliding the head and tail pointers.

===============      Bugs      ===============

The Java monitor methods are documented at
http://docs.oracle.com/javase/6/docs/api/java/lang/Object.html#wait(),
I was able to use the provided code pattern as a guide for insert()
and remove() to create my implementation without copying a solution.

Some other obstacles I had were relearning Java syntax, especially for
generics, the Thread API, and understanding where to use the final
keyword with the synchronized keyword for thread safety.

One shortcoming of this implementation is the use of notifyAll()
instead of a single notification system. Producer and consumer threads
end up waiting in the same queue waiting for conditions that are
respectively orthogonal. An improvement would be to explicitly use
Lock and Condition objects so that when the condition of the buffer
changed, only a thread of the appropriate type would be notified.
Furthermore, as threads queued to wait for a condition, they could be
notified specifically and fairly in FIFO-order. Overall, the finer
granularity of using Lock and Condition objects reduces the number of
context switches. These context switches - where nothing is added or
removed from the buffer - are seen in the output when a clustered
series of Producer or Consumer threads happen to be scheduled together
after their respective condition becomes false by a leading thread.
