//
// Application.java
//

import java.util.LinkedList;
import java.util.Random;

public class Application {
    private static LinkedList<Producer> producers;
    private static LinkedList<Consumer> consumers;
    private static BoundedBuffer<Integer> buffer;
    private static Random generator;
    private static int timeToSleep;
    private static int numProducers;
    private static int numConsumers;

    public static void main(String args[]) {
        /* parse command-line arguments */
        if (args.length == 3) {
            timeToSleep  = Integer.parseInt(args[0]);
            numProducers = Integer.parseInt(args[1]);
            numConsumers = Integer.parseInt(args[2]);

            System.out.printf("timeToSleep  = %d\n", timeToSleep);
            System.out.printf("numProducers = %d\n", numProducers);
            System.out.printf("numConsumers = %d\n", numConsumers);
        } else {
            System.err.println("Usage error.");
            return;
        }

        /* initialize bounded buffer */
        buffer = new BoundedBuffer<Integer>();
        generator = new Random();

        /* create producer threads */
        producers = new LinkedList<Producer>();
        System.out.printf("Creating %d producer threads.\n", numProducers);
        for (int i = 0; i < numProducers; i++) {
            String name = "Producer" + i;
            producers.add(new Producer(name, generator, buffer));
        }

        /* create consumer threads */
        consumers = new LinkedList<Consumer>();
        System.out.printf("Creating %d consumer threads.\n", numConsumers);
        for (int i = 0; i < numConsumers; i++) {
            String name = "Consumer" + i;
            consumers.add(new Consumer(name, generator, buffer));
        }

        /* sleep */
        try {
            System.out.println("Main thread sleeping.");
            Thread.sleep(timeToSleep);
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted.");
        }

        System.exit(0);
    }
}
