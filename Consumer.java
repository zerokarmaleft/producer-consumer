//
// Consumer.java
//

import java.util.Random;

public class Consumer implements Runnable {
    private Random generator;
    private BoundedBuffer<Integer> buffer;
    private Thread thread;

    public Consumer(String name, Random generator,
                    BoundedBuffer<Integer> buffer) {
        this.generator = generator;
        this.buffer = buffer;
        this.thread = new Thread(this, name);
        this.thread.start();
    }

    public void run() {
        /* consume numbers from bounded buffer forever */
        while (true) {
            try {
                int n = (Integer) buffer.remove();
            } catch (InterruptedException e) {
                System.out.println("Consumer was interrupted.");
            }
        }
    }
}
