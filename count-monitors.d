#!/usr/sbin/dtrace -Zs

dtrace::

hotspot*:::monitor-contended-enter
{
        self->ts = timestamp;
}

hotspot*:::monitor-contended-entered
/ self->ts /
{
        @[tid] = quantize(timestamp - self->ts);
        self->ts = 0;
}
