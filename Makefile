all:
	javac -Xlint:unchecked Application.java Producer.java Consumer.java BoundedBuffer.java
	jar cvfe assignment-5.jar Application *.class

test: all
	java -jar assignment-5.jar 5000 5 5

clean:
	rm -rf *.class *.jar
